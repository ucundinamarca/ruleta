var Game_UdeC = {
    url: "https://virtual.ucundinamarca.edu.co/udecvirtual/set_juegos_app/quiz/add",
    api: "https://virtual.ucundinamarca.edu.co/service_udec.php",
    envio:false,
    data: {
        user:     "User Default",
        photo:    "none",
        quiz:     "",
        game:     "",
        url_game: ""
    },
    save: function () {
        if(this.envio==false){
            this.envio=true;
            if(this.data.user==undefined){
                this.data.user="User default";
            }
            if(this.data.photo==undefined){
                this.data.photo="User default";
            }
            this.data.quiz=JSON.stringify(this.data.quiz);
            $.ajax({
                type: "POST",
                url: this.url,
                data: this.data,
                success: function(response)
                {
                    var jsonData = JSON.parse(response);
               },
               error: function(error) {
                alert('Error al guardar');
                console.log(error);
              }
           });
        }
    },
    info_moodle: function () {
        var t =this;
        $.ajax({
            type: "POST",
            url: this.api,
            success: function(response)
            {
                var jsonData = JSON.parse(response);
                t.data.photo=jsonData.picture;
                t.data.user=jsonData.firstname+" "+jsonData.lastname;
                console.log(t.data);
           },
           error: function(error) {
            alert('Error al cargar datos de Moodle por favor lóguese en la plataforma');
            console.log(error);
          }
       });
    }
}