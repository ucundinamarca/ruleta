
var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Ruleta",
    autor: "Edilson Laverde Molina",
    date:  "10/03/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",      name:"clic"   },
    {url:"sonidos/fail.mp3",       name:"fail"   },
    {url:"sonidos/good.mp3",       name:"good"   },
    {url:"sonidos/ruleta.mp3",     name:"ruleta" }
];

var terminos=[
    {texto:"HONESTIDAD"},
    {texto:"DILIGENCIA"},
    {texto:"RESPETO"},
    {texto:"COMPROMISO"},
    {texto:"JUSTICIA"},
    {texto:"TRANSPARENCIA"},
    {texto:"LEALTAD"}
];







index=0;

var primary=true;
var puntos=0;
var p=0;
function main(sym) {

udec = ivo.structure({
        created:function(){
           t=this;
           //ivo(ST+"m").text("0");
            //precarga audios//
           ivo.load_audio(audios,onComplete=function(){
               ivo(ST+"preload").hide();
               ivo(ST+"b").html(`<textarea name="texto" id="texto" rows="10" cols="50"></textarea>`);
               t.animation();
               t.events();
               stage1.play();
               ivo(".ins").hide();
               ivo(ST+"page_1").show();
               Game_UdeC.info_moodle();
           });
        },
        methods: {
            ruleta:function(){
                p=Math.floor(Math.random() * (5000 - 1) + 1)+p
                let t=Math.random() * (5 - 1) + 1;
                TweenMax.to(ST+"cir", t,        {rotation:p,ease:Bounce.easeOut});
                TweenMax.to(ST+"v", t/5,        {rotation:10,repeat:4,onComplete:function(){
                    TweenMax.to(ST+"v", t/5,        {rotation:0});
                    stage3.timeScale(1).play();
                }});
                ivo(ST+"tg1 p").text(`${terminos[index].texto}`);
                index+=1;
            },
            events:function(){
                var t=this;
                ivo(ST+"icon1").on("click",function(){
                    ivo(".ins").hide();
                    ivo(ST+"page_1").show();
                    ivo.play("clic");
                });

                $(ST+"icon2").addClass("animated infinite tada");
                ivo(ST+"icon2").on("click",function(){
                    $(ST+"icon2").removeClass("animated infinite tada");
                    ivo(".ins").hide();
                    ivo(ST+"page_2").show();
                    ivo.play("clic");
                });
                
                ivo(ST+"btn_comenzar").on("click",function(){
                   
                        stage1.reverse().timeScale(3);
                        stage2.play().timeScale(1);
                        primary=false;
                        ivo.play("clic");
                    
                      
                });
                ivo(ST+"ruleta").on("click",function(){
         
                    if(index<7){
                        $("#texto").val("");
                        t.ruleta();
                        ivo.play("ruleta");
                    }else{
                        var 
                        doc = new jsPDF();
                        let html=`<h1>Reto de integridad</h1>
                        <table>
                            <tr>
                                <td>Termino</td>
                                <td>Respuesta</td>
                            </tr>
                            <tr>
                                <td>${terminos[0].texto}</td>
                                <td>${terminos[0].reg}</td>
                            </tr>
                            <tr>
                                <td>${terminos[1].texto}</td>
                                <td>${terminos[1].reg}</td>
                            </tr>
                            <tr>
                                <td>${terminos[2].texto}</td>
                                <td>${terminos[2].reg}</td>
                            </tr>
                            <tr>
                                <td>${terminos[3].texto}</td>
                                <td>${terminos[3].reg}</td>
                            </tr>
                            <tr>
                                <td>${terminos[4].texto}</td>
                                <td>${terminos[4].reg}</td>
                            </tr>
                            <tr>
                                <td>${terminos[5].texto}</td>
                                <td>${terminos[5].reg}</td>
                            </tr>
                            <tr>
                                <td>${terminos[6].texto}</td>
                                <td>${terminos[6].reg}</td>
                            </tr>
                        </table>`;
                        doc.fromHTML(html, 15, 15, {
                          'width': 800
                        });
                        Swal.fire({
                            title: '<strong>Retroalimentación</strong>',imageUrl: './images/home.png',
                            imageHeight: 100,
                            imageAlt: 'A tall image',
                            showCancelButton: false,
                            showConfirmButton: false,
                            html:
                              'El reconocimiento y la adopción de los valores nos permitirá responder con altura a las circunstancias y a lo que se espera de nosotros como servidores públicos."' ,
                            
                          }); 
                          ivo(".swal2-image").on("click",function(){
                            window.location.href="https://virtual.ucundinamarca.edu.co/course/view.php?id=15147&section=1";
                          }).css("cursor","pointer");
                        doc.save('resumen.pdf');
                        console.log(terminos);
                        Game_UdeC.data.quiz=terminos;
                        Game_UdeC.data.game="Ruleta";
                        Game_UdeC.data.url_game='https://virtual.ucundinamarca.edu.co/red/Otros/games/ruleta/ruleta.html';
                        Game_UdeC.save();
                        
                        
                    }
                });
               
                ivo(ST+"save").on("click",function(){
                    stage3.timeScale(3).reverse();
                    terminos[index-1].reg=$("#texto").val();
                    ivo.play("clic");
                });
                ivo(ST+"close").on("click",function(){
                    stage4.timeScale(3).reverse();
                    ivo.play("clic");
                });
                ivo(ST+"btn_objetivo").on("click",function(){
                    stage4.timeScale(1).play();
                    ivo.play("clic");
                });
                /*

                ivo(ST+"si").on("click",function(){
                    index+=1;
                    ivo.play("fail");
                    t.launch();
                });
                ivo(ST+"no").on("click",function(){
                    index+=1;
                    ivo.play("good");
                    puntos+=1;
                    ivo(ST+"p p span").text(puntos);
                    t.launch();
                });
                
                ivo(".a").on("click",function(){
                    let id1 = "#"+ivo(this).attr("id")+"_";
                    let id2 = "#"+ivo(this).attr("id")+"__";
                    stage2.reverse().timeScale(4);
                    stage3.play().timeScale(1);
                    ivo.play("clic");
                    ivo(".fotos").hide();
                    ivo(id1).show();
                    ivo(id2).show();
                    t.launch();
                });*/
            },
            animation:function(){
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST+"stage1", .8,          {x:1300,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"msg", .8,             {x:1300,opacity:0}), 0);
                stage1.stop();

                stage2 = new TimelineMax({onComplete:function(){
                    $(ST+"ruleta").trigger("click");
                    
                }});
                stage2.append(TweenMax.from(ST+"stage2", .8,          {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"box1", .8,            {y:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"btn_obj", .8,         {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"ruleta", .8,          {y:1300,opacity:0}), 0);
                stage2.stop();

                stage3 = new TimelineMax();
                stage3.append(TweenMax.fromTo(ST+"stage3", .4,        {opacity:0,y:900},{opacity:1,y:0}), 0);
                stage3.append(TweenMax.from(ST+"g1", .4,              {opacity:0,x:900}), 0);
                stage3.append(TweenMax.from(ST+"g2", .4,              {opacity:0,x:900}), 0);
                stage3.stop();

                stage4 = new TimelineMax();
                stage4.append(TweenMax.fromTo(ST+"stage4", .4,        {opacity:0,y:900},{opacity:1,y:0}), 0);
                stage4.append(TweenMax.from(ST+"close", .4,              {opacity:0,x:900}), 0);
                stage4.stop();
 

            }
        }
 });
}


