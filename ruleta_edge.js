/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Recurso_1',
                            type: 'image',
                            rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px']
                        },
                        {
                            id: 'fondo-1',
                            type: 'image',
                            rect: ['-1px', '2px', '1024px', '640px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"fondo-1.jpg",'0px','0px']
                        },
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['72', '95', '880', '450', 'auto', 'auto'],
                            c: [
                            {
                                id: 'msg',
                                type: 'group',
                                rect: ['0', '0', '880', '450', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_15',
                                    type: 'image',
                                    rect: ['0px', '0px', '880px', '450px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px']
                                },
                                {
                                    id: 'btn_comenzar',
                                    type: 'group',
                                    rect: ['368', '350', '201', '46', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    c: [
                                    {
                                        id: 'btn_comenzar_',
                                        type: 'image',
                                        rect: ['0px', '0px', '201px', '46px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_14.png",'0px','0px']
                                    },
                                    {
                                        id: 'Rectangle2Copy',
                                        type: 'rect',
                                        rect: ['28px', '9px', '148px', '28px', 'auto', 'auto'],
                                        fill: ["rgba(140,198,63,1.00)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"]
                                    },
                                    {
                                        id: 'Text4Copy',
                                        type: 'text',
                                        rect: ['18px', '9px', '164px', '24px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"font-size: 18px; color: rgb(255, 255, 255);\">Comenzar</span></p>",
                                        align: "left",
                                        font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(28,41,47,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                },
                                {
                                    id: 'page_2',
                                    type: 'group',
                                    rect: ['70', '179', '705', '85', 'auto', 'auto'],
                                    userClass: "ins",
                                    c: [
                                    {
                                        id: 'Text2',
                                        type: 'text',
                                        rect: ['0px', '-37px', '705px', '85px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\"><span style=\"font-size: 35px;\">Instrucción</span></p><p style=\"margin: 0px;\"><span style=\"font-size: 35px;\">​</span></p><p style=\"margin: 0px;\">Dé clic sobre la ruleta y escriba una frase sobre el valor que obtenga.</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\">​</p>",
                                        align: "center",
                                        font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                },
                                {
                                    id: 'page_1',
                                    type: 'group',
                                    rect: ['70', '88px', '705', '85', 'auto', 'auto'],
                                    userClass: "ins",
                                    c: [
                                    {
                                        id: 'Text2Copy',
                                        type: 'text',
                                        rect: ['0px', '0px', '705px', '85px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​Este juego está enmarcado en unos principios de acción puestos en términos de “qué hago como servidor público íntegro” y “qué no hago”. El objetivo es entender los valores como una característica propia, y que lleve al servidor a una reflexión sobre cuáles comportamientos de la cotidianidad debe ajustar, basado en el principio de la existencia de un “yo” que decide y actúa íntegramente. Debemos tener presente que ser servidor público no es cualquier labor. Ser servidor público implica un comportamiento especial, un deber ser particular y una manera específica de actuar, siempre orientada bajo el sentido de la ética de lo público.</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\">​</p>",
                                        align: "center",
                                        font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                },
                                {
                                    id: 'icon1',
                                    type: 'ellipse',
                                    rect: ['450px', '325px', '12px', '13px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    borderRadius: ["50%", "50%", "50%", "50%"],
                                    fill: ["rgba(192,155,155,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'icon2',
                                    type: 'ellipse',
                                    rect: ['475px', '325px', '12px', '13px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    borderRadius: ["50%", "50%", "50%", "50%"],
                                    fill: ["rgba(192,155,155,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                }]
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['22px', '37px', '979', '532', 'auto', 'auto'],
                            c: [
                            {
                                id: 'ruleta',
                                type: 'group',
                                rect: ['-7px', '72px', '418', '517', 'auto', 'auto'],
                                cursor: 'pointer',
                                c: [
                                {
                                    id: 'Recurso_3',
                                    type: 'image',
                                    rect: ['0px', '0px', '418px', '517px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px']
                                },
                                {
                                    id: 'cir',
                                    type: 'image',
                                    rect: ['18px', '18px', '385px', '385px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_6.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_10',
                                    type: 'image',
                                    rect: ['175px', '450px', '70px', '35px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_10.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_7',
                                    type: 'image',
                                    rect: ['4px', '14px', '411px', '403px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                                },
                                {
                                    id: 'v',
                                    type: 'image',
                                    rect: ['195px', '-15px', '75px', '67px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_42.png",'0px','0px']
                                },
                                {
                                    id: 'alumbra2',
                                    symbolName: 'alumbra',
                                    type: 'rect',
                                    rect: ['4', '13', '411', '403', 'auto', 'auto']
                                }]
                            },
                            {
                                id: 'box1',
                                type: 'image',
                                rect: ['589px', '-21px', '394px', '301px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"box1.png",'0px','0px']
                            },
                            {
                                id: 'btn_obj',
                                type: 'image',
                                rect: ['625px', '613px', '201px', '46px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px']
                            },
                            {
                                id: 'btn_objetivo',
                                type: 'image',
                                rect: ['107px', '-99px', '207px', '52px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"objetivo.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage3',
                            type: 'group',
                            rect: ['0', '164', '1008', '435', 'auto', 'auto'],
                            c: [
                            {
                                id: 'g2',
                                type: 'group',
                                rect: ['512', '124px', '496', '326', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'tg2',
                                    type: 'text',
                                    rect: ['15px', '52px', '442px', '49px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">​Escriba aquí su respuesta</p>",
                                    userClass: "game",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                },
                                {
                                    id: 'cajon',
                                    type: 'image',
                                    rect: ['-1px', '21px', '472px', '275px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"cajon.png",'0px','0px']
                                },
                                {
                                    id: 'b',
                                    type: 'rect',
                                    rect: ['14px', '94px', '436px', '163px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"]
                                },
                                {
                                    id: 'save',
                                    type: 'group',
                                    rect: ['309', '296', '148', '40', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    c: [
                                    {
                                        id: 'save_',
                                        type: 'image',
                                        rect: ['0px', '0px', '148px', '40px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"save.png",'0px','0px']
                                    },
                                    {
                                        id: 'Rectangle2',
                                        type: 'rect',
                                        rect: ['18px', '7px', '113px', '28px', 'auto', 'auto'],
                                        fill: ["rgba(140,198,63,1.00)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"]
                                    },
                                    {
                                        id: 'Text4',
                                        type: 'text',
                                        rect: ['8px', '7px', '132px', '24px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"font-size: 18px; color: rgb(255, 255, 255);\">Guardar</span></p>",
                                        align: "left",
                                        font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(28,41,47,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                },
                                {
                                    id: 'Rectangle',
                                    type: 'rect',
                                    rect: ['44px', '47px', '379px', '39px', 'auto', 'auto'],
                                    fill: ["rgba(234,234,234,1.00)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"]
                                },
                                {
                                    id: 'Text',
                                    type: 'text',
                                    rect: ['14px', '52px', '443px', '28px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">​Escriba aquí su respuesta&nbsp;</p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(28,41,47,1.00)", "normal", "none", "", "break-word", ""]
                                }]
                            },
                            {
                                id: 'g1',
                                type: 'group',
                                rect: ['0', '0', '473', '195', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_11',
                                    type: 'image',
                                    rect: ['0px', '0px', '473px', '195px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_11.png",'0px','0px']
                                },
                                {
                                    id: 'tg1',
                                    type: 'text',
                                    rect: ['16px', '52px', '442px', '78px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "center",
                                    userClass: "game",
                                    font: ['Arial, Helvetica, sans-serif', [34, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            }]
                        },
                        {
                            id: 'stage4',
                            type: 'group',
                            rect: ['267', '168', '502', '233', 'auto', 'auto'],
                            c: [
                            {
                                id: 'box_obj',
                                type: 'image',
                                rect: ['0px', '38px', '473px', '195px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"box_obj.png",'0px','0px']
                            },
                            {
                                id: 'close',
                                type: 'image',
                                rect: ['426px', '0px', '76px', '76px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"close.png",'0px','0px']
                            },
                            {
                                id: 'Text3',
                                type: 'text',
                                rect: ['33px', '67px', '411px', '132px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\"><span style=\"font-weight: 900; font-size: 18px;\">Objetivo​</span></p><p style=\"margin: 0px;\">Con el fin de realizar una revisión periódica de la implementación del código autonómico de la universidad se creó un test basado en la caja de herramientas de función pública con el cual se pretende conocer la percepción de ustedes nuestro activo más preciado (los funcionarios), sobre la implementación e interiorización del código autonómico (código de integridad) de la entidad.&nbsp;</p>",
                                align: "left",
                                userClass: "",
                                font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '272px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 2000,
                    autoPlay: true,
                    data: [
                        [
                            "eid5",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "easeInSine",
                            "${v}",
                            [28,4],
                            [28,4],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid236",
                            "-moz-transform-origin",
                            0,
                            0,
                            "easeInSine",
                            "${v}",
                            [28,4],
                            [28,4],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid237",
                            "-ms-transform-origin",
                            0,
                            0,
                            "easeInSine",
                            "${v}",
                            [28,4],
                            [28,4],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid238",
                            "msTransformOrigin",
                            0,
                            0,
                            "easeInSine",
                            "${v}",
                            [28,4],
                            [28,4],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid239",
                            "-o-transform-origin",
                            0,
                            0,
                            "easeInSine",
                            "${v}",
                            [28,4],
                            [28,4],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid240",
                            "transform-origin",
                            0,
                            0,
                            "easeInSine",
                            "${v}",
                            [28,4],
                            [28,4],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ]
                    ]
                }
            },
            "alumbra": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: 'alumbra',
                            opacity: '1',
                            rect: ['0px', '0px', '411px', '403px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/alumbra.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '411px', '403px']
                        }
                    }
                },
                timeline: {
                    duration: 2000,
                    autoPlay: true,
                    data: [
                        [
                            "eid123",
                            "opacity",
                            0,
                            1000,
                            "linear",
                            "${alumbra}",
                            '1',
                            '0'
                        ],
                        [
                            "eid125",
                            "opacity",
                            1000,
                            1000,
                            "linear",
                            "${alumbra}",
                            '0',
                            '1'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("ruleta_edgeActions.js");
})("EDGE-32082485");
